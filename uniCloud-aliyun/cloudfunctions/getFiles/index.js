'use strict';
const db = uniCloud.database() //对数据库的对象获取；
exports.main = async (event, context) => {
	 const collection = db.collection('file_list') //对holle数据库的获取；
	//event为客户端上传的参数
	console.log('event : ', event)
	
	// 接收的查询条件，文件类型
	const type = event.type
	
	let res = []
	// 添加数据
	if(type) {
		res = await collection.where({
			type: type
		}).get()
	} else {
		res = await collection.get()
	}
	
	const fileList = res.data
	
	//返回数据给客户端
	return {
		code: 200,
		msg: '查询成功',
		data: fileList
	}
};
