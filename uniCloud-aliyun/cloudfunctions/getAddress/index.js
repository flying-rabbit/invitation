'use strict';
const db = uniCloud.database() //对数据库的对象获取；
exports.main = async (event, context) => {
	const collection = db.collection('address_list')
	
	const { data } = await collection.get()
	
	const returnData = data || []
	
	//返回数据给客户端
	return {
		code: 200,
		msg: '查询成功',
		data: returnData[0] || {}
	}
};
