/**
 * 节流函数封装
 * @fun 所执行的函数
 * @wait 节流时间
 */
export const throttleFun = function(fun:void,wait:number) {
	let timer: NodeJS.Timer | null = null;
	return ()=>{
		if(timer) {
			return
		}
		timer = setTimeout(()=>{
			fun()
			timer = null
		},wait)
	}
}
