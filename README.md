# invitation

#### 介绍
这是一个自己使用的邀请涵

#### 软件架构
软件架构说明：使用了uniapp+uniCloud服务架构，其实主要用这个云存储文件


#### 安装教程

1.  自己创建云服务空间关联后即可使用
2.  在云服务控制台上创建file_list数据表，云函数使用的这个，可以自己改，改了上传运行
3.  表结构为，name，url，type即可，type为了区分音频与图片

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
